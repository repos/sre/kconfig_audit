#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0
"""
audit kernel config settings
"""

import logging

from argparse import ArgumentParser
from pathlib import Path


def get_args() -> None:
    """Parse arguments.

    Returns:
        `argparse.Namespace`: The parsed argparser Namespace
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument("-A", "--arch", choices=('x86_64',), default='x86_64')
    parser.add_argument("config_file")
    return parser.parse_args()


def get_log_level(args_level: int) -> int:
    """Convert an integer to a logging log level.

    Arguments:
        args_level (int): The log level as an integer

    Returns:
        int: the logging loglevel
    """
    return {
        0: logging.ERROR,
        1: logging.WARN,
        2: logging.INFO,
        3: logging.DEBUG,
    }.get(args_level, logging.DEBUG)


def load_file(path: Path) -> dict:
    """Load a config file and parse it to a dictionary"""
    data = {}
    for line in path.read_text().splitlines():
        line = line.strip()
        if not line or line[0] == '#':
            continue
        key, value = line.split('=', 1)
        data[key] = value
    return data


def compare(expected: dict, actual: dict) -> dict:
    """compare the two hashes."""
    missing_keys = expected.keys() - actual.keys()
    differences = {}
    for key, value in expected.items():
        if key not in actual or actual[key] == value:
            continue
        differences[key] = {
            'expected': value,
            'actual': actual[key]
        }
    return {
        'missing': sorted(missing_keys),
        'differences': dict(sorted(differences.items())),
    }


def display_report(report: dict) -> None:
    """Pretty print the report."""
    if not report['missing'] and not report['differences']:
        print("All recomendations applied.")
        return
    if report['missing']:
        print('==== The following recomend keys are not configured')
        for missing in report['missing']:
            print(f'* {missing}')
    if report['differences']:
        print('==== The following recomend options have different settings ')
        for key, data in report['differences'].items():
            print(f"* {key}: expected {data['expected']}, got {data['actual']}")


def main() -> int:
    """Main entry point.

    Returns:
        int: an int representing the exit code
    """
    exit_code = 0
    script_dir = Path(__file__).parent
    config_dir = script_dir / 'configs'
    args = get_args()
    logging.basicConfig(level=get_log_level(args.verbose))
    expected = load_file(config_dir / 'defaults.txt')
    expected |= load_file(config_dir / f'{args.arch}.txt')
    actual = load_file(Path(args.config_file))
    report = compare(expected, actual)
    display_report(report)

    return exit_code


if __name__ == "__main__":
    raise SystemExit(main())
